# Open Linux Browser Tab WITH...

A Linux bash implementation of my Windows based [Open Browser Tab WITH..](https://gitlab.com/yman84/open-browser-tab-with) script. 

## ## Requirements

- [xdotool](https://github.com/jordansissel/xdotool) - For sending keyboard events

- [xclip](https://github.com/astrand/xclip) - For accessing clipboard contents to grab browser URL

## To-Do

- [ ] Flesh out description

- [ ] Add Example keyboard shortcut configuration with keymapper

- [ ] Add Preview image/gif
