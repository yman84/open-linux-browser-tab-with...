#!/bin/bash

#------------------------------------------------
# SETTINGS AND VARIABLES
#-----------------------------------------------
# save passed in argument as a variable
command=${1}

# Pocket Settings
PocketAccessToken=""
PocketConsumerKey=""

# Telegram Settings
TelegramChatID=""
TelegramBotToken=""

# KDE Connect Settings
KDEConnectDeviceOne=""
KDEConnectDeviceTwo=""
KDEConnectDeviceThree=""

# yt-dlp Settings
VideoDownloadFolder="/home/${USER}/Downloads"
AudioDownloadFolder="/home/${USER}/Downloads"
VideoResolution="720" # 480 OR 720 OR 1080 OR etc..
AudioQuality="Best"  # Best OR MP3

# System terminal used for yt-dlp progress popups
TerminalApplication="alacritty"

#------------------------------------------------
# Script
#-----------------------------------------------

# display short notification of what command was passed in
notify-send -t 1000 "$command"

# backup clipboard contents
clipBackup=`xclip -o -selection clipboard`


# Get URL of active tab through xdotool
sleep .1 ; xdotool key "Escape" ; xdotool key ctrl+l ; sleep .1 ; xdotool key ctrl+l ; sleep .1
xdotool key ctrl+c
sleep .25
clip=`xclip -o -selection clipboard`


# Execute action based on passed in command
case "$command" in 
  *archiveorg*)
      NewURL="https://web.archive.org/web/2/"${clip} ;
      xdotool type ${NewURL} ;
      xdotool key Enter ;;

  *brave*) 
      brave ${clip} ;;

  *braveincognito*) 
      brave --incognito ${clip} ;;

  *chromium*) 
      chromium ${clip} ;;

  *chromiumincognito*) 
      chromium --incognito ${clip} ;;

  *chrome*) 
      chrome ${clip} ;;

  *chromeincognito*) 
      chrome --incognito ${clip} ;;

  *librewolf*) 
      librewolf ${clip} ;;

  *librewolfprivate*) 
      librewolf -private ${clip} ;;

  *firefox*) 
      firefox ${clip} ;;

  *KDEConnectDeviceOne*) 
      kdeconnect-cli -n "${KDEConnectDeviceOne}" --share "${clip}" ;;

  *KDEConnectDeviceTwo*) 
      kdeconnect-cli -n "${KDEConnectDeviceTwo}" --share "${clip}" ;;

  *mpv720*) 
      mpv --ytdl-format="bestvideo[ext=mp4][height<=?720]+bestaudio[ext=m4a]" ${clip} ;;

  *mpv480*) 
      mpv --ytdl-format="bestvideo[ext=mp4][height<=?480]+bestaudio[ext=m4a]" ${clip} ;;

  *ytdlp_video*) 
      ${TerminalApplication} -e yt-dlp -f 'bestvideo[height<=720]+bestaudio' --embed-thumbnail --add-metadata --no-mtime -o "${VideoDownloadFolder}/%(title)s.%(ext)s" ${clip} & ;;

  *ytdlp_audio*) 
      ${TerminalApplication} -e yt-dlp -f bestaudio --extract-audio --embed-thumbnail --add-metadata --no-mtime -o "${VideoDownloadFolder}/%(title)s.%(ext)s" ${clip} & ;;

  *pocket*) 
      curl \
      --header "Content-type: application/json" \
      --data '{"url":"'${clip}'","consumer_key":"'${PocketConsumerKey}'","access_token":"'${PocketAccessToken}'"}' \
      https://getpocket.com/v3/add ;;

  *telegram*) 
      curl -s "https://api.telegram.org/bot${TelegramBotToken}/sendMessage?chat_id=${TelegramChatID}&text=${clip}" ; notify-send -t 500 "Telegram Successful";;    
  
  *clipboardurl*) 
      exit ;;
esac

# Restore clipboard contents
echo $clipBackup | xclip -sel c   # copy variable contents to clip
